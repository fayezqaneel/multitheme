# -*- coding: utf-8 -*-

from odoo import models, fields, api

class WebsiteConfigSettings(models.TransientModel):
	_inherit = 'website.config.settings'

	#add custom field to website settings @fayez
	default_theme = fields.Selection(related='website_id.default_theme')
