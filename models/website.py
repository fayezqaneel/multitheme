# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Website(models.Model):
	_inherit = 'website'

	#adding default theme field and its selection method which return the installed themes of the system @fayez
	default_theme = fields.Selection(selection='list_themes')

	#serving default theme field @fayez
	@api.multi
	def list_themes(self):
		themes = self.env['ir.module.module'].search([('name', 'like',"theme_" ),("state", "=","installed")])
		theme_names = []
		for theme in themes:
			theme_names.append(theme.name)
		return zip(theme_names,theme_names)
