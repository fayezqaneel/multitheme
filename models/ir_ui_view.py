# -*- coding: utf-8 -*-

from odoo import models, fields, api


#filter out theme specific views @fayez
class View(models.Model):
	_inherit = 'ir.ui.view'

	@api.model
	def get_inheriting_views_arch(self, view_id, model):
		"""Retrieves the architecture of views that inherit from the given view, from the sets of
		views that should currently be used in the system. During the module upgrade phase it
		may happen that a view is present in the database but the fields it relies on are not
		fully loaded yet. This method only considers views that belong to modules whose code
		is already loaded. Custom views defined directly in the database are loaded only
		after the module initialization phase is completely finished.

		:param int view_id: id of the view whose inheriting views should be retrieved
		:param str model: model identifier of the inheriting views.
		:rtype: list of tuples
		:return: [(view_arch,view_id), ...]
		"""
		user_groups = self.env.user.groups_id
		conditions = [
			['inherit_id', '=', view_id],
			['model', '=', model],
			['mode', '=', 'extension'],
			['active', '=', True]
		]
		if self.pool._init and not self._context.get('load_all_views'):
			# Module init currently in progress, only consider views from
			# modules whose code is already loaded

			# Search terms inside an OR branch in a domain
			# cannot currently use relationships that are
			# not required. The root cause is the INNER JOIN
			# used to implement it.
			modules = tuple(self.pool._init_modules) + (self._context.get('install_mode_data', {}).get('module'),)
			views = self.search(conditions + [('model_ids.module', 'in', modules)])
			views = self.search(conditions + [('id', 'in', list(self._context.get('check_view_ids') or (0,)) + map(int, views))])
		else:
			views = self.search(conditions)

		#filter out theme specific views @fayez
		website_id = self.env.context.get('website_id')

		if website_id:
			default_theme = self.env['website'].browse(website_id).default_theme
			filtered_views = []
			for view in views.sudo():
				if default_theme in view.key or not "theme_" in view.key:
					filtered_views.append(view)
			return [(view.arch, view.id) for view in filtered_views if not view.groups_id or (view.groups_id & user_groups)]
		else:
			return [(view.arch, view.id) for view in views.sudo() if not view.groups_id or (view.groups_id & user_groups)]