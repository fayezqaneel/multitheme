# -*- coding: utf-8 -*-
{
    'name': "Multi-Theme Support",
    'sequence': 95,
    'summary': "Adding support for assigning a theme for each a website",
    'description': "Enable user to select a theme for each website",
    'version': '1.0',
    'author': "Fayez Saeed Qandeel",
    'website': "http://www.bloopark.de",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Website',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/res_config_views.xml'
    ],
    'auto_install': True,
    'installable': True,
    'application': True
}